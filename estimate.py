theLatency = 0.2

alpha = 13.01
bandwidthRatio = 0.97

windowSize = 4194304   *  8 # bits
data_size = 2000000000 *  8 # bits

bandwidth = 1e8

throughputWithLatency =  (windowSize / (2. * theLatency)) * bandwidthRatio # only in version 3.28 it seems to be multiplied by the bandwidth factor
throughputWithBandwidth = (bandwidthRatio * bandwidth)/1.05

print(f"throughputWithLatency:   {throughputWithLatency}")
print(f"throughputWithBandwidth: {throughputWithBandwidth}")

throughput = min(throughputWithLatency, throughputWithBandwidth)
estimatedTime =   alpha * transferLatency + data_size / throughput

print(f"estimatedTime: {estimatedTime}")