#include "simgrid/s4u.hpp"
#include <iostream>
#include <stdlib.h>
#include <cmath>

XBT_LOG_NEW_DEFAULT_CATEGORY(example, "Messages specific for this example");


static void get_message(simgrid::s4u::Host *dest)
{
    simgrid::s4u::Mailbox *mb = simgrid::s4u::Mailbox::by_name(dest->get_name());
    mb->get();
    std::cout << "Time = "  << simgrid::s4u::Engine::get_clock() <<  std::endl;
}

static void master()
{
  simgrid::s4u::Host * h2 = simgrid::s4u::Host::by_name("B");
  simgrid::s4u::Mailbox *mb = simgrid::s4u::Mailbox::by_name(h2->get_name());
  simgrid::s4u::Actor::create("ator",h2,get_message,h2);
  mb->put_init(new double(0),2000000000)->detach();    
}

int main(int argc, char* argv[])
{
  simgrid::s4u::Engine e(&argc, argv);
  e.load_platform("plat.xml");
  std::vector<simgrid::s4u::Host*> hosts = simgrid::s4u::Engine::get_instance()->get_all_hosts();
  simgrid::s4u::Actor::create("master", simgrid::s4u::Host::by_name("A"),master);
  e.run();
  return 0;
}